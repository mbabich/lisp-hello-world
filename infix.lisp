(in-package #:lisp-hello-world)

(defun parse-infix-list (infix-list)
  (labels ((parse-sublist (sublist last-cons symbol)
             (destructuring-bind (first second &rest tail)
                 sublist
               (unless (and (symbolp first)
                            (or (not symbol)
                                (eql symbol first)))
                 (error "Invalid operator ~S" first))
               (let* ((second* (if (consp second) (parse-infix-list second) second))
                      (last-cons* (list second*)))
                 (setf (cdr last-cons) last-cons*)
                 (if tail
                     (parse-sublist tail last-cons* first)
                     first)))))
    (destructuring-bind (zeroth &rest tail)
        infix-list
      (let ((zeroth* (if (consp zeroth) (parse-infix-list zeroth) zeroth)))
        (if tail
            (let ((prefix-list (list zeroth*)))
              (cons (parse-sublist tail prefix-list nil) prefix-list))
            zeroth*)))))

(defmacro infix (&body body)
  (parse-infix-list body))

(defun %infix-reader-macro (stream char count)
  (declare (ignore char count))
  (let ((first-char (read-char stream)))
    (if (char= first-char #\()
        (with-input-from-string (start "(infix ")
          (with-open-stream (s (make-concatenated-stream start stream))
            (read s)))
        (error "An infix reader macro must be parenthesized."))))

(defun infix-reader-macro ()
  (make-dispatch-macro-character #\# #\i)
  (set-dispatch-macro-character #\# #\i #'%infix-reader-macro))
