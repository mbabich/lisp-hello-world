(in-package #:lisp-hello-world)

(defpackage #:std)

(defmacro << (output &body body)
  `(symbol-macrolet ((std::cout *standard-output*)
                     (std::endl (progn (terpri) (finish-output))))
     (let ((*standard-output* ,output))
       ,@(loop :for item :in body
               :if (stringp item)
                 :collect `(format t ,item)
               :else
                 :collect item))))
