(asdf:defsystem #:lisp-hello-world
  :description "The worst hello world for Common Lisp."
  :version "1.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://gitlab.com/mbabich/lisp-hello-world"
  :bug-tracker "https://gitlab.com/mbabich/lisp-hello-world/issues"
  :source-control (:git "https://gitlab.com/mbabich/lisp-hello-world.git")
  :components ((:file "package")
               (:file "infix")
               (:file "cout")
               (:file "main")))
