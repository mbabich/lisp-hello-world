(in-package #:lisp-hello-world)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (infix-reader-macro))

(defun hello ()
  #I(std::cout << "Hello, " << "world!" << std::endl))
